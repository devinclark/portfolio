import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "@/views/Home.vue";
import Experience from "@/views/Experience.vue";
import Projects from "@/views/Projects.vue";
import Skills from "@/views/Skills.vue";
import NotFound from "@/views/NotFound.vue";
import { RouteMetaEntity } from "@/entities/RouteMetaEntity";
import { Constants } from "@/constants/Constants";

Vue.use(VueRouter);

export const Routes: Array<RouteConfig> = [
    {
        path: "/",
        name: "Home",
        component: Home,
        meta: new RouteMetaEntity(Constants.defaultRouteMeta, ["Full Stack Developer"], "Devin Keith Clark's personal profile site", "Devin Keith Clark", undefined, undefined, undefined, undefined, undefined, undefined, false)
    },
    {
        path: "/experience",
        name: "Experience",
        component: Experience,
        meta: new RouteMetaEntity(Constants.defaultRouteMeta, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, true)
    },
    {
        path: "/projects",
        name: "Projects",
        component: Projects,
        meta: new RouteMetaEntity(Constants.defaultRouteMeta, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, true)
    },
    {
        path: "/skills",
        name: "Skills",
        component: Skills,
        meta: new RouteMetaEntity(Constants.defaultRouteMeta, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, true)
    },
    {
        path: "*",
        name: "Not Found",
        component: NotFound,
        meta: new RouteMetaEntity(Constants.defaultRouteMeta, undefined, undefined, undefined, undefined, "none", undefined, undefined, undefined, undefined, false)
    }
];

const Router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: Routes
});

export default Router;
