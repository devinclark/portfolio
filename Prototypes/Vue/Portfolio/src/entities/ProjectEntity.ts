export class ProjectEntity {
    id: string;
    name: string;
    applicationType: string;
    company: string;
    employmentCompanyId?: string;
    projectUrl?: string;
    projectImages: Array<string>;
    startDate: Date;
    endDate?: Date;
    highlights: Array<string>;
    technologies: Array<string>;

    constructor(id: string, name: string, applicationType: string, company: string, projectImageCount: number, startDate: Date, highlights: Array<string>, technologies: Array<string>, employmentCompanyId?: string, projectUrl?: string, endDate?: Date) {
        this.id = id;
        this.name = name;
        this.applicationType = applicationType;
        this.company = company;
        this.employmentCompanyId = employmentCompanyId;
        this.projectUrl = projectUrl;
        this.projectImages = new Array<string>();
        this.startDate = startDate;
        this.endDate = endDate;
        this.highlights = highlights;
        this.technologies = technologies;

        for (let i = 1; i <= projectImageCount; i++) {
            this.projectImages.push(this.getImagePath(i));
        }
    }

    private getImagePath(imageIndex: number): string {
        return require(`@/assets/projects/${this.id}/${imageIndex}.jpg`)
    }
}