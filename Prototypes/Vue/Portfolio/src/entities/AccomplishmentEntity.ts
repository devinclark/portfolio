export class AccomplishmentEntity {
    id: string;
    award: string;
    organization: string;
    organizationUrl?: string;
    organizationAvatarUrl: string;
    location: string;
    date: Date;

    constructor(id: string, award: string, organization: string, date: Date, location: string, organizationUrl?: string) {
        this.id = id;
        this.award = award;
        this.organization = organization;
        this.organizationUrl = organizationUrl;
        this.organizationAvatarUrl = require(`@/assets/accomplishments/${id}.jpg`);
        this.location = location;
        this.date = date;
    }
}