export class SkillEntity {
    id: string;
    name: string;
    category: string;
    activeLearningDate?: Date;
    experienceYears?: number;
    sourceUrl?: string;
    version?: string;

    constructor(id: string, name: string, category: string, activeLearningDate?: Date, experienceYears?: number, sourceUrl?: string, version?: string) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.activeLearningDate = activeLearningDate;
        this.experienceYears = experienceYears;
        this.sourceUrl = sourceUrl;
        this.version = version;
    }

    public getExperienceMonths(): number {
        let months = this.experienceYears ? this.experienceYears * 12 : 0;
        const today = new Date();

        if (this.activeLearningDate) {
            months += ((today.getFullYear() - this.activeLearningDate.getFullYear()) * 12) + today.getMonth() - this.activeLearningDate.getMonth();
        }

        return months;
    }
}