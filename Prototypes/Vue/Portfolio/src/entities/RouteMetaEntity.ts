export class RouteMetaEntity {
    keywords?: Array<string>
    description?: string
    title?: string
    language?: string
    robots?: string
    canonical?: string
    author?: string
    shortcutIcon?: string
    contentLanguages?: Array<string>
    isOnMainMenu: boolean

    constructor(defaultRouteMeta?: RouteMetaEntity, keywords?: Array<string>, description?: string, title?: string, language?: string, robots?: string, canonical?: string, author?: string, shortcutIcon?: string, contentLanguages?: Array<string>, isOnMainMenu?: boolean) {
        this.keywords = keywords ?? defaultRouteMeta?.keywords;
        this.description = description ?? defaultRouteMeta?.description;
        this.title = title ?? defaultRouteMeta?.title;
        this.language = language ?? defaultRouteMeta?.language;
        this.robots = robots ?? defaultRouteMeta?.robots;
        this.canonical = canonical ?? defaultRouteMeta?.canonical;
        this.author = author ?? defaultRouteMeta?.author;
        this.shortcutIcon = shortcutIcon ?? defaultRouteMeta?.shortcutIcon;
        this.contentLanguages = contentLanguages ?? defaultRouteMeta?.contentLanguages;
        this.isOnMainMenu = isOnMainMenu ?? false;
    }
}