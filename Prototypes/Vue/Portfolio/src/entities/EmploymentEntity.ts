export class EmploymentEntity {
    id: string;
    title: string;
    employmentType: string;
    company: string;
    companyUrl?: string;
    companyAvatarUrl: string;
    location: string;
    startDate: Date;
    endDate?: Date;
    responsibilities: Array<string>;
    technologies: Array<string>;
    tools: Array<string>;
    concepts: Array<string>;

    constructor(id: string, title: string, employmentType: string, company: string, location: string, startDate: Date, responsibilities: Array<string>, technologies: Array<string>, tools: Array<string>, concepts: Array<string>, companyUrl?: string, endDate?: Date) {
        this.id = id;
        this.title = title;
        this.employmentType = employmentType;
        this.company = company;
        this.companyUrl = companyUrl;
        this.companyAvatarUrl = require(`@/assets/companies/${id}.jpg`);
        this.location = location;
        this.startDate = startDate;
        this.endDate = endDate;
        this.responsibilities = responsibilities;
        this.technologies = technologies;
        this.tools = tools;
        this.concepts = concepts;
    }
}