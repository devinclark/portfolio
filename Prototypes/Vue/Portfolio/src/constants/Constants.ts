import { RouteMetaEntity } from '@/entities/RouteMetaEntity';
import { EmploymentEntity } from '@/entities/EmploymentEntity';
import { ProjectEntity } from '@/entities/ProjectEntity';
import { AccomplishmentEntity } from '@/entities/AccomplishmentEntity';
import { SkillEntity } from '../entities/SkillEntity';

export class Constants {
    public static defaultRouteMeta: RouteMetaEntity = new RouteMetaEntity(
        undefined,
        undefined,
        undefined,
        "Devin Keith Clark",
        "en-us",
        "all",
        undefined,
        "Devin Keith Clark",
        "/favicon.ico"
    );
    public static employments: Array<EmploymentEntity> = new Array<EmploymentEntity>(
        new EmploymentEntity(
            "bright-health",
            "Software Developer III",
            "Full-time",
            "Bright Health",
            "Remote",
            new Date(2020, 7),
            new Array<string>("Started a new adventure!"),
            new Array<string>(),
            new Array<string>(),
            new Array<string>(),
            "https://brighthealthplan.com"
        ),
        new EmploymentEntity(
            "flagship-credit",
            "Senior Software Developer",
            "Full-time",
            "Flagship Credit",
            "Irvine, CA",
            new Date(2017, 4),
            new Array<string>(
                "Developed <strong>Single Page Applications</strong> using <strong>React</strong> optimized for <strong>Responsive Design</strong> and <strong>SEO</strong>.",
                "Implemented <strong>White Labeling</strong> into our public web applications.",
                "Developed <strong>REST</strong> APIs as a <strong>Microservices</strong> architecture using <strong>.NET Core</strong>.",
                "Refactored several monolithic legacy .NET Framework systems into decoupled <strong>.NET Core Nuget</strong> packages.",
                "Designed and implemented workflows for integrating with external services such as analyzing soft pull credit reports, approving loan applications, searching for vehicles, and more.",
                "Created and maintained <strong>Azure</strong> services.",
                "Created <strong>Azure Devops</strong> pipelines to support <strong>Continuous Integration</strong>.",
                "Designed team's <strong>Git</strong> branching strategy.",
                "Created custom services to automate tasks such as deleting merged <strong>Git</strong> branches, determining user stories in a release, merging production code into all other branches, and more.",
                "Created new <strong>Splunk</strong> instance and implemented central logging with all services.",
                "Took ownership of <strong>DevOps</strong> challenges and <strong>Epics</strong> by managing the full <strong>SDLC</strong>.",
                "Mentored junior developers and presented training sessions about new technologies.",
                "Lead interview process when our team did not have a manager.",
            ),
            new Array<string>(".NET Core 2.2", "Entity Framework", "MSSQL Server", "React", "TypeScript", "LESS", "HTML5"),
            new Array<string>("Visual Studio", "Azure", "Azure DevOps", "Bitbucket", "Splunk", "Postman", "Full Story", "JIRA"),
            new Array<string>("Microservices Architecture", "Multithreaded Development", "Single Page Applications", "Dependency Injection", "REST", "Responsive Design", "SEO", "Continuous Integration"),
            "https://flagshipcredit.com",
            new Date(2020, 2)
        ),
        new EmploymentEntity(
            "tallan",
            "Software Consultant",
            "Full-time",
            "Tallan",
            "Santa Ana, CA",
            new Date(2015, 2),
            new Array<string>(
                "Developed front-end web views using <strong>AngularJS</strong> optimized for <strong>Responsive Design</strong> and <strong>SEO</strong>.",
                "Developed back-end components using <strong>MVC5</strong> and <strong>Entity Framework</strong>.",
                "Improved database performance by refactoring slow queries and caching content.",
                "Analyzed client software requirements and recommended solutions to technical limitations.",
                "Mentored junior developers and updated company training materials.",   
                "Conducted interviews for junior developers."
            ),
            new Array<string>(".NET Framework 4.6", "Entity Framework", "MSSQL Server", "Angular", "LESS", "HTML5"),
            new Array<string>("Visual Studio", "Bitbucket", "Postman"),
            new Array<string>("Single Page Applications", "Dependency Injection", "REST", "Responsive Design", "SEO"),
            "https://tallan.com",
            new Date(2017, 0)
        ),
        new EmploymentEntity(
            "yor-health",
            "Software Developer",
            "Full-time",
            "YOR Health",
            "Irvine, CA",
            new Date(2013, 4),
            new Array<string>(
                "Developed front-end web views using <strong>jQuery</strong> optimized for performance and <strong>SEO</strong>.",
                "Developed back-end components using <strong>Webforms</strong> and <strong>Stored Procedures</strong>.",
                "Refactored several web applications to use <strong>REST</strong> APIs and <strong>AJAX</strong>.",
                "Transformed designer mock-ups into pixel perfect front-end code.",
                "Worked in an <strong>Agile</strong> environment adapting to changing project requirements." 
            ),
            new Array<string>(".NET Framework 4.5", "MSSQL Server", "jQuery", "CSS3", "HTML4"),
            new Array<string>("Visual Studio", "Git", "JIRA"),
            new Array<string>("REST", "SEO"),
            "https://yorhealth.com",
            new Date(2015, 2)
        )
    ).sort((a, b) => (b.endDate ? b.endDate.getTime() : new Date().getTime()) - a.startDate.getTime());
    public static projects: Array<ProjectEntity> = new Array<ProjectEntity>(
        new ProjectEntity(
            "modcars",
            "ModCars",
            "Website",
            "Flagship Credit",
            3,
            new Date(2019, 2),
            new Array<string>(
                "<strong>ModCars</strong> was designed to tranform the standard auto dealer buying process into an online shopping experience.",
                "Consumers could use the product to search for vehicles from any dealership and filter the results to match their needs.",
                "Vehicle inventory was obtained using an external service that is already integrated with most dealers.",
                "The trade-in tool could be used to estimate the value of the customer's current vehicle and apply the value to their new purchase.",
                "Other optional products could be purchased such as <strong>Gap</strong> insurance, extended warranties, maintenance plans, and more.",
                "Consumers could apply for financing and view pre-approved loan offers within minutes.",
                "Consumers could compare the offers from each lender and choose the best one."
            ),
            new Array<string>(".NET Core 2.2", "Entity Framework", "MSSQL Server", "React", "TypeScript", "LESS", "HTML5"),
            "flagship-credit",
            undefined,
            new Date(2020, 2)
        ),
        new ProjectEntity(
            "carfinance",
            "CarFinance",
            "Website",
            "Flagship Credit",
            2,
            new Date(2017, 5),
            new Array<string>(
                "<strong>CarFinance</strong> was designed to provide an easy auto loan qualification procoess.",
                "Consumers could apply for a loan by providing their personal information and view pre-approved loan offers within seconds.",
                "The initial offers were decisioned by the customer's <strong>soft pull credit report</strong>, which will not affect their credit score.",
                "Lenders provided their approval requirements to the company, which allowed the site to quickly decision offers.",
                "Consumers could compare the offers from each lender and choose the best one.",
            ),
            new Array<string>(".NET Core 2.1", "Entity Framework", "MSSQL Server", "React", "TypeScript", "LESS", "HTML5"),
            "flagship-credit",
            undefined,
            new Date(2019, 11)
        ),
        new ProjectEntity(
            "free-credit-score",
            "Free Credit Score",
            "Website",
            "Experian",
            3,
            new Date(2015, 3),
            new Array<string>(
                "<strong>Free Credit Report</strong> is one of several <strong>Experian</strong> websites to provide credit report details and other services to consumers.",
                "Consumers could view their report for free once per year, but could pay a subscription to view their report more frequently.",
                "Paid subscribers got access to additional services such as <strong>Identity Protection</strong>, <strong>Dark Web Monitoring</strong>, and more.",
                "Consumers could also pay to view their credit reports from other credit bureaus."
            ),
            new Array<string>("AngularJS", "LESS", "HTML5"),
            "tallan",
            "https://freecreditscore.com",
            new Date(2016, 0)
        ),
        new ProjectEntity(
            "smartraiser",
            "SmartRaiser",
            "Website",
            "SmartRaiser",
            3,
            new Date(2016, 1),
            new Array<string>(
                "<strong>SmartRaiser</strong> was a fundraising mobile app for school team activities that enabled customized fundraising campaigns.",
                "Team members could reach out to local businesses who were interested in participating in their fundraising campaign.",
                "Businesses would provide a deal or discount to be a part of a fundraising campaign.",
                "Team members would reach out to others to support their school team by making a donation to the campaign.",
                "Donaters could view the business deals that participated with the campaign.",
                "The mobile app allows consumers to donate to other campaigns and receive notifications when a participating business was nearby."
            ),
            new Array<string>(".NET Framework 4.6", "Entity Framework", "MSSQL Server", "AngularJS", "LESS", "HTML5"),
            "tallan",
            undefined,
            new Date(2016, 10)
        ),
        new ProjectEntity(
            "yor-office",
            "YOR Office",
            "Website",
            "YOR Health",
            3,
            new Date(2013, 4),
            new Array<string>(
                "<strong>YOR Office</strong> is designed to manage account details and reports for <strong>YOR Health Individual Representatives</strong>.",
                "Consumers could use the product to track their sales objectives, coordinate activies with their other team members, view training materials, and more."
            ),
            new Array<string>(".NET Framework 4.5", "MSSQL Server", "jQuery", "CSS3", "HTML4"),
            "yor-health",
            "https://yorhealth.com/admin",
            new Date(2015, 2)
        )
    ).sort((a, b) => (b.endDate ? b.endDate.getTime() : new Date().getTime()) - a.startDate.getTime());
    public static accomplishments: Array<AccomplishmentEntity> = new Array<AccomplishmentEntity>(
        new AccomplishmentEntity(
            "cal-state-university-san-marcos",
            "B.S. Computer Science",
            "CSU San Marcos",
            new Date(2012, 11),
            "San Marcos, CA",
            "https://csusm.edu"
        ),
        new AccomplishmentEntity(
            "boy-scouts-of-america",
            "Eagle Scout",
            "Boy Scouts of America",
            new Date(2004, 0),
            "Rancho Santa Margarita, CA",
            "https://scouting.org"
        )
    ).sort((a, b) => b.date.getTime() - a.date.getTime());
    public static skills: Array<SkillEntity> = new Array<SkillEntity>(
        new SkillEntity(
            "net-core",
            ".NET Core",
            "Back-End Development",
            new Date(2017, 7),
            undefined,
            "https://dotnet.microsoft.com/learn/dotnet/what-is-dotnet",
            "3.1"
        ),
        new SkillEntity(
            "entity-framework",
            "Entity Framework",
            "Back-End Development",
            new Date(2016, 1),
            undefined,
            "https://docs.microsoft.com/en-us/ef"
        ),
        new SkillEntity(
            "mssql-server",
            "MSSQL Server",
            "Database Development",
            new Date(2013, 5),
            undefined,
            "https://www.microsoft.com/en-us/sql-server"
        ),
        new SkillEntity(
            "react",
            "React",
            "Front-End Development",
            new Date(2018, 3),
            undefined,
            "https://reactjs.org"
        ),
        new SkillEntity(
            "typescript",
            "Typescript",
            "Front-End Development",
            new Date(2018, 3),
            undefined,
            "https://www.typescriptlang.org"
        ),
        new SkillEntity(
            "less",
            "LESS",
            "Front-End Development",
            new Date(2015, 3),
            undefined,
            "http://lesscss.org"
        ),
        new SkillEntity(
            "html",
            "HTML",
            "Front-End Development",
            new Date(2013, 4),
            undefined,
            "https://www.w3.org/html",
            "5"
        ),
        new SkillEntity(
            "git",
            "Git",
            "Development Tools",
            new Date(2013, 5),
            undefined,
            "https://git-scm.com"
        ),
        new SkillEntity(
            "net-framework",
            ".NET Framework",
            "Back-End Development",
            new Date(2013, 5),
            undefined,
            "https://dotnet.microsoft.com/learn/dotnet/what-is-dotnet-framework",
            "4.7"
        ),
        new SkillEntity(
            "jquery",
            "jQuery",
            "Front-End Development",
            new Date(2013, 6),
            undefined,
            "https://jquery.com"
        ),
        new SkillEntity(
            "css",
            "CSS",
            "Front-End Development",
            new Date(2013, 5),
            undefined,
            "https://www.w3.org/Style/CSS/Overview.en.html",
            "3"
        ),
        new SkillEntity(
            "visual-studio",
            "Visual Studio",
            "Development Tools",
            new Date(2013, 4),
            undefined,
            "https://visualstudio.microsoft.com"
        ),
        new SkillEntity(
            "jira",
            "JIRA",
            "External Services",
            new Date(2017, 4),
            2,
            "https://www.atlassian.com/software/jira"
        ),
        new SkillEntity(
            "rest",
            "REST",
            "Concepts",
            new Date(2014, 4)
        ),
        new SkillEntity(
            "seo",
            "SEO",
            "Concepts",
            new Date(2014, 4)
        ),
        new SkillEntity(
            "angular-js",
            "AngularJS",
            "Front-End Development",
            undefined,
            2,
            "https://angularjs.org",
            "1.3"
        ),
        new SkillEntity(
            "bitbucket",
            "Bitbucket",
            "Development Tools",
            new Date(2015, 5),
            undefined,
            "https://bitbucket.org/product"
        ),
        new SkillEntity(
            "postman",
            "Postman",
            "Development Tools",
            new Date(2015, 5),
            undefined,
            "https://www.postman.com"
        ),
        new SkillEntity(
            "single-page-applications",
            "Single Page Applications",
            "Concepts",
            new Date(2015, 4)
        ),
        new SkillEntity(
            "dependency-injection",
            "Dependency Injection",
            "Concepts",
            new Date(2016, 2)
        ),
        new SkillEntity(
            "responsive-design",
            "Responsive Design",
            "Concepts",
            new Date(2015, 4)
        ),
        new SkillEntity(
            "azure",
            "Azure",
            "Cloud Engineering",
            new Date(2019, 1),
            undefined,
            "https://azure.microsoft.com"
        ),
        new SkillEntity(
            "azure-devops",
            "Azure DevOps",
            "DevOps",
            new Date(2019, 10),
            undefined,
            "https://azure.microsoft.com/en-us/solutions/devops"
        ),
        new SkillEntity(
            "splunk",
            "Splunk",
            "External Services",
            undefined,
            1,
            "https://www.splunk.com"
        ),
        new SkillEntity(
            "full-story",
            "Full Story",
            "External Services",
            undefined,
            1,
            "https://www.fullstory.com"
        ),
        new SkillEntity(
            "microservices-architecture",
            "Microservices Architecture",
            "Concepts",
            new Date(2017, 5)
        ),
        new SkillEntity(
            "multithreaded-development",
            "Multithreaded Development",
            "Concepts",
            new Date(2017, 5)
        ),
        new SkillEntity(
            "continuous-integration",
            "Continous Integration",
            "DevOps",
            new Date(2018, 4)
        ),
        new SkillEntity(
            "vue",
            "Vue",
            "Front-End Development",
            new Date(2020, 4),
            undefined,
            "https://vuejs.org"
        ),
        new SkillEntity(
            "docker",
            "Docker",
            "Cloud Engineering",
            new Date(2020, 7),
            undefined,
            "https://www.docker.com"
        ),
        new SkillEntity(
            "slack",
            "Slack",
            "External Services",
            new Date(2019, 8),
            undefined,
            "https://slack.com"
        )
    ).sort((a, b) => b.getExperienceMonths() - a.getExperienceMonths());
}