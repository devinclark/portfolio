﻿namespace DKC.Portfolio.Web
{
    internal static class Employments
    {
        internal static readonly IEnumerable<EmploymentEntity> History = new List<EmploymentEntity>()
        {
            new EmploymentEntity()
            {
                Title = "Senior Software Developer",
                EmploymentType = EmploymentEntity.EmploymentTypes.FullTime,
                Company = "CarMax",
                Location = "Remote",
                StartDate = new DateTime(2021, 10, 18),
                Accomplishments = new List<string>()
                {
                    "Leveraged <strong>.NET 6</strong> to develop real-time solutions dedicated to the identification of fraudulent activities.",
                    "Crafted comprehensive workflows for detecting a wide spectrum of fraudulent activities, encompassing OFAC compliance, device-based identification, identity verification, and more.",
                    "Assumed end-to-end ownership of projects, meticulously managing the entire Software Development Life Cycle.",
                    "Spearheaded the creation of <strong>Azure DevOps</strong> deployment pipelines and infrastructure pipelines utilizing <strong>Bicep</strong>.",
                    "Engineered <strong>Microservices</strong> by architecting <strong>REST</strong> APIs with <strong>.NET 6</strong>.",
                    "Established and upheld <strong>Azure</strong> resources to support project requirements.",
                    "Vigilantly monitored production resources using <strong>Application Insights</strong> and promptly addressed any outages.",
                    "Provided guidance and mentorship to junior developers and delivered informative presentations on emerging technologies.",
                    "Led the interview process for various development positions, ensuring the selection of qualified candidates."
                },
                Technologies = new List<string>()
                {
                    ".NET 6",
                    "NoSQL",
                    "Bicep"
                },
                Tools = new List<string>()
                {
                    "Visual Studio",
                    "Azure",
                    "Azure DevOps",
                    "Git",
                    "Application Insights",
                    "Postman",
                    "LeanKit",
                    "Microsoft Teams",
                    "Pager Duty"
                },
                Concepts = new List<string>()
                {
                    "Microservices",
                    "Functions",
                    "Multithreaded Development",
                    "Dependency Injection",
                    "REST",
                    "Continuous Integration",
                    "IAC"
                },
                CompanyURL = "https://carmax.com"
            },
            new EmploymentEntity()
            {
                Title = "Software Developer III",
                EmploymentType = EmploymentEntity.EmploymentTypes.FullTime,
                Company = "Bright Health",
                Location = "Remote",
                StartDate = new DateTime(2020, 8, 10),
                EndDate = new DateTime(2021, 10, 15),
                Accomplishments = new List<string>()
                {
                    "Developed tailored solutions on the <strong>DevEx</strong> team using <strong>.NET 5</strong> to automate labor-intensive internal processes.",
                    "Collaborated with cross-functional teams to identify pain points and devised workflow automation solutions.",
                    "Established and upheld <strong>Azure</strong> resources, ensuring their continuous availability.",
                    "Proactively monitored production resources and swiftly responded to outages to maintain maximum uptime.",
                    "Designed and implemented <strong>Azure DevOps</strong> pipeline templates to facilitate <strong>Continuous Integration</strong> for other teams.",
                    "Orchestrated the migration of existing cloud infrastructure into <strong>ARM Templates</strong> to enrich our <strong>Infrastructure As Code</strong> repository.",
                    "Diagnosed and resolved DevOps-related issues encountered by other teams."
                },
                Technologies = new List<string>()
                {
                    ".NET 5",
                    "Docker",
                    "ARM Templates",
                    "Kubernetes"
                },
                Tools = new List<string>()
                {
                    "Visual Studio",
                    "Azure",
                    "Azure DevOps",
                    "Git",
                    "Postman",
                    "JIRA",
                    "Slack",
                    "Splunk On-Call"
                },
                Concepts = new List<string>()
                {
                    "Microservices",
                    "Multithreaded Development",
                    "Dependency Injection",
                    "REST",
                    "Continuous Integration",
                    "IAC"
                },
                CompanyURL = "https://brighthealthgroup.com"
            },
            new EmploymentEntity()
            {
                Title = "Senior Software Developer",
                EmploymentType = EmploymentEntity.EmploymentTypes.FullTime,
                Company = "Flagship Credit",
                Location = "Irvine, CA",
                StartDate = new DateTime(2017, 5, 8),
                EndDate = new DateTime(2020, 3, 20),
                Accomplishments = new List<string>()
                {
                    "Spearheaded the development of <strong>Responsive Design</strong> and <strong>SEO</strong> optimized <strong>Single Page Applications</strong> using <strong>React</strong>.",
                    "Successfully integrated <strong>White Labeling</strong> capabilities into our public-facing web applications.",
                    "Engineered <strong>Microservices</strong> by creating <strong>REST</strong> APIs with <strong>.NET Core.</strong>",
                    "Transformed legacy monolithic <strong>.NET Framework</strong> systems into decoupled <strong>.NET Core</strong> Nuget packages.",
                    "Designed and executed workflows for seamless integration with various tools, including soft pull credit report analysis, loan application approval, vehicle searches, and more.",
                    "Established and managed <strong>Azure</strong> resources, ensuring robust infrastructure support.",
                    "Orchestrated <strong>Azure DevOps</strong> pipelines to facilitate <strong>Continuous Integration</strong> processes.",
                    "Architected the team's <strong>Git</strong> branching strategy, streamlining version control.",
                    "Pioneered <strong>DevEx</strong> services to automate tasks such as branch deletion, user story tracking in releases, and production code merging across branches.",
                    "Implemented a new <strong>Splunk</strong> instance and centralized logging across all services.",
                    "Assumed full ownership of <strong>DevOps</strong> challenges and Epics, managing the complete Software Development Life Cycle.",
                    "Mentored junior developers and conducted training sessions to disseminate knowledge about emerging technologies.",
                    "Led the interview process in the absence of a team manager, ensuring the recruitment of top-tier talent."
                },
                Technologies = new List<string>()
                {
                    ".NET Core 2.2",
                    "Entity Framework",
                    "MSSQL Server",
                    "React",
                    "TypeScript",
                    "LESS",
                    "HTML5"
                },
                Tools = new List<string>()
                {
                    "Visual Studio",
                    "Azure",
                    "Azure DevOps",
                    "Bitbucket",
                    "Splunk",
                    "Postman",
                    "Full Story",
                    "JIRA"
                },
                Concepts = new List<string>()
                {
                    "Microservices",
                    "Multithreaded Development",
                    "Single Page Applications",
                    "Dependency Injection",
                    "REST",
                    "Responsive Design",
                    "SEO",
                    "Continuous Integration"
                },
                CompanyURL = "https://flagshipcredit.com"
            },
            new EmploymentEntity()
            {
                Title = "Software Consultant",
                EmploymentType = EmploymentEntity.EmploymentTypes.FullTime,
                Company = "Tallan",
                Location = "Santa Ana, CA",
                StartDate = new DateTime(2015, 3, 24),
                EndDate = new DateTime(2017, 1, 9),
                Accomplishments = new List<string>()
                {
                    "Designed and engineered responsive front-end web interfaces with <strong>AngularJS</strong> and emphasizing <strong>SEO</strong>.",
                    "Built robust back-end components using <strong>MVC5</strong> and harnessed the capabilities of <strong>Entity Framework</strong>.",
                    "Boosted database performance by optimizing sluggish queries and implementing content caching strategies.",
                    "Evaluated client software requirements, offering innovative solutions to overcome technical constraints.",
                    "Provided mentorship to junior developers and contributed to the enhancement of company training resources.",
                    "Conducted interviews for prospective junior developer candidates, ensuring the selection of top talent."
                },
                Technologies = new List<string>()
                {
                    ".NET Framework 4.6",
                    "Entity Framework",
                    "MSSQL Server",
                    "AngularJS",
                    "LESS",
                    "HTML5"
                },
                Tools = new List<string>()
                {
                    "Visual Studio", 
                    "Bitbucket", 
                    "Postman"
                },
                Concepts = new List<string>()
                {
                    "Single Page Applications", 
                    "Dependency Injection", 
                    "REST", 
                    "Responsive Design", 
                    "SEO"
                },
                CompanyURL = "https://tallan" +
                ".com"
            },
            new EmploymentEntity()
            {
                Title = "Software Developer",
                EmploymentType = EmploymentEntity.EmploymentTypes.FullTime,
                Company = "YOR Health",
                Location = "Irvine, CA",
                StartDate = new DateTime(2013, 5, 20),
                EndDate = new DateTime(2015, 3, 20),
                Accomplishments = new List<string>()
                {
                    "Enhanced user experience by crafting performant and SEO-friendly front-end web interfaces, leveraging the power of <strong>jQuery</strong>.",
                    "Engineered back-end modules through the utilization of <strong>Webforms</strong> and <strong>Stored Procedures</strong>.",
                    "Modernized multiple web applications by integrating <strong>REST</strong> APIs and <strong>AJAX</strong> for enhanced functionality.",
                    "Translated designer concepts into flawless, pixel-perfect front-end code.",
                    "Thrived in an <strong>Agile</strong> work environment, adeptly adjusting to evolving project demands."
                },
                Technologies = new List<string>()
                {
                    ".NET Framework 4.5", 
                    "MSSQL Server", 
                    "jQuery", 
                    "CSS3", 
                    "HTML4"
                },
                Tools = new List<string>()
                {
                    "Visual Studio", 
                    "Git", 
                    "JIRA"
                },
                Concepts = new List<string>()
                {
                    "REST", 
                    "SEO"
                },
                CompanyURL = "https://yorhealth.com"
            }
        }.OrderByDescending(x => x.EndDate ?? DateTime.UtcNow);

        internal static readonly int CareerExperienceYears = History.Sum(x => x.GetExperienceMonths()) / 12;
    }
}
