﻿namespace DKC.Portfolio.Web
{
    internal static class SEO
    {
        internal static readonly MetaEntity DefaultMETAData = new MetaEntity(false)
        {
            TitleSuffix = "Devin Clark",
            Language = "en-us",
            Robots = "all",
            Author = "Devin Clark",
            ShortcutIcon = "/favicon.ico"
        };
    }
}
