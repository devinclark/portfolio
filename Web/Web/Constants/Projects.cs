﻿namespace DKC.Portfolio.Web
{
    internal static class Projects
    {
        internal static readonly IEnumerable<ProjectEntity> History = new List<ProjectEntity>()
        {
            new ProjectEntity()
            {
                Name = "ModCars",
                Subtitle = "Transforming Auto Dealership Shopping Online",
                ApplicationType = "Website",
                Company = "Flagship Credit",
                EmploymentCompanyID = "flagship-credit",
                StartDate = new DateTime(2019, 3, 1),
                EndDate = new DateTime(2020, 3, 1),
                ImageQuantity = 3,
                Highlights = new List<string>()
                {
                    "Modernized auto dealership shopping with an immersive online experience.",
                    "Enhanced vehicle search with multi-dealership access and advanced filtering.",
                    "Seamlessly integrated external service for comprehensive vehicle inventory.",
                    "User-friendly trade-in valuation tool for accurate vehicle value estimation.",
                    "Offered optional products like Gap insurance and warranties for customization.",
                    "Streamlined financing with quick loan approval and transparent lender comparison."
                },
                Technologies = new List<string>()
                {
                    ".NET Core 2.2", 
                    "Entity Framework", 
                    "MSSQL Server", 
                    "React", 
                    "TypeScript", 
                    "LESS", 
                    "HTML5"
                }
            },
            new ProjectEntity()
            {
                Name = "CarFinance",
                Subtitle = "Effortless Auto Loan Qualification",
                ApplicationType = "Website",
                Company = "Flagship Credit",
                EmploymentCompanyID = "flagship-credit",
                StartDate = new DateTime(2017, 6, 1),
                EndDate = new DateTime(2019, 12, 1),
                ImageQuantity = 2,
                Highlights = new List<string>()
                {
                    "Simplified the auto loan qualification process for consumers.",
                    "Rapid loan application with instant pre-approved offers.",
                    "Utilized soft pull credit reports for credit score protection.",
                    "Implemented smart decisioning based on lender approval criteria.",
                    "Empowered users to compare and select the best loan offers."
                },
                Technologies = new List<string>()
                {
                    ".NET Core 2.1", 
                    "Entity Framework", 
                    "MSSQL Server", 
                    "React", 
                    "TypeScript", 
                    "LESS", 
                    "HTML5"
                }
            },
            new ProjectEntity()
            {
                Name = "Free Credit Score",
                Subtitle = "Empowering Financial Insight",
                ApplicationType = "Website",
                Company = "Experian",
                EmploymentCompanyID = "tallan",
                StartDate = new DateTime(2015, 4, 1),
                EndDate = new DateTime(2016, 1, 1),
                ImageQuantity = 3,
                Highlights = new List<string>()
                {
                    "Part of the Experian portfolio, providing credit report details and services to consumers.",
                    "Offered free annual credit reports, with subscription options for more frequent access.",
                    "Subscribers enjoyed enhanced services like Identity Protection and Dark Web Monitoring.",
                    "Enabled users to access credit reports from various credit bureaus, promoting comprehensive financial awareness."
                },
                Technologies = new List<string>()
                {
                    "AngularJS", 
                    "LESS", 
                    "HTML5"
                },
                ProjectURL = "https://freecreditscore.com"
            },
            new ProjectEntity()
            {
                Name = "SmartRaiser",
                Subtitle = "Easy School Team Fundraising",
                ApplicationType = "Website",
                Company = "SmartRaiser",
                EmploymentCompanyID = "tallan",
                StartDate = new DateTime(2016, 2, 1),
                EndDate = new DateTime(2016, 11, 1),
                ImageQuantity = 3,
                Highlights = new List<string>()
                {
                    "Created a website for school teams to conduct customized fundraising campaigns.",
                    "Facilitated team outreach to local businesses interested in campaign participation.",
                    "Engaged businesses by offering deals and discounts as part of fundraising efforts.",
                    "Enabled team members to solicit donations from supporters to benefit their school teams.",
                    "Provided donors with visibility into participating business deals within campaigns.",
                    "Encouraged community engagement by allowing donations to various campaigns and sending location-based notifications for nearby participating businesses."
                },
                Technologies = new List<string>()
                {
                    ".NET Framework 4.6", 
                    "Entity Framework", 
                    "MSSQL Server", 
                    "AngularJS", 
                    "LESS", 
                    "HTML5"
                }
            },
            new ProjectEntity()
            {
                Name = "YOR Office",
                Subtitle = "Enabling YOR Health Representatives",
                ApplicationType = "Website",
                Company = "YOR Health",
                EmploymentCompanyID = "yor-health",
                StartDate = new DateTime(2013, 5, 1),
                EndDate = new DateTime(2015, 3, 1),
                ImageQuantity = 3,
                Highlights = new List<string>()
                {
                    "Developed a platform for YOR Health Individual Representatives to manage accounts and reports.",
                    "Streamlined sales objective tracking and team collaboration.",
                    "Provided easy access to training materials and resources.",
                    "Enhanced the productivity and effectiveness of YOR Health representatives."
                },
                Technologies = new List<string>()
                {
                    ".NET Framework 4.5", 
                    "MSSQL Server", 
                    "jQuery", 
                    "CSS3", 
                    "HTML4"
                },
                ProjectURL = "https://yorhealth.com/admin"
            }
        }.OrderByDescending(x => x.EndDate ?? DateTime.UtcNow);
    }
}
