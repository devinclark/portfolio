﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DKC.Portfolio.Web
{
    internal static class Accomplishments
    {
        internal static readonly IEnumerable<AccomplishmentEntity> History = new List<AccomplishmentEntity>()
        {
            new AccomplishmentEntity()
            {
                ID = "cal-state-university-san-marcos",
                Award = "B.S. Computer Science",
                Organization = "CSU San Marcos",
                Date = new DateTime(2012, 12, 1),
                Location = "San Marcos, CA",
                OrganizationURL = "https://csusm.edu"
            },
            new AccomplishmentEntity()
            {
                ID = "boy-scouts-of-america",
                Award = "Eagle Scout",
                Organization = "Boy Scouts of America",
                Date = new DateTime(2004, 1, 1),
                Location = "Rancho Santa Margarita, CA",
                OrganizationURL = "https://scouting.org"
            }
        }.OrderByDescending(x => x.Date);
    }
}
