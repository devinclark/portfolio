﻿namespace DKC.Portfolio.Web
{
    internal static class Skills
    {
        internal static readonly IEnumerable<SkillEntity> History = new List<SkillEntity>()
        {
            new SkillEntity()
            {
                Name = "Entity Framework",
                Category = Categories.BackEndDevelopment,
                ExperienceYears = 4,
                SourceURL = "https://docs.microsoft.com/en-us/ef"
            },
            new SkillEntity()
            {
                Name = "SQL Server",
                Category = Categories.DatabaseDevelopment,
                ExperienceYears = 7,
                SourceURL = "https://www.microsoft.com/en-us/sql-server"
            },
            new SkillEntity()
            {
                Name = "React",
                Category = Categories.FrontEndDevelopment,
                ExperienceYears = 3,
                SourceURL = "https://reactjs.org"
            },
            new SkillEntity()
            {
                Name = "Typescript",
                Category = Categories.FrontEndDevelopment,
                ExperienceYears = 2,
                SourceURL = "https://www.typescriptlang.org"
            },
            new SkillEntity()
            {
                Name = "LESS",
                Category = Categories.FrontEndDevelopment,
                ExperienceYears = 5,
                SourceURL = "http://lesscss.org"
            },
            new SkillEntity()
            {
                Name = "HTML",
                Category = Categories.FrontEndDevelopment,
                ExperienceYears = 7,
                SourceURL = "https://www.w3.org/html",
                Version = "5"
            },
            new SkillEntity()
            {
                Name = "Git",
                Category = Categories.DevelopmentTools,
                ActiveLearningDate = new DateTime(2013, 6, 1),
                SourceURL = "https://git-scm.com"
            },
            new SkillEntity()
            {
                Name = ".NET",
                Category = Categories.BackEndDevelopment,
                ActiveLearningDate = new DateTime(2013, 6, 1),
                SourceURL = "https://dotnet.microsoft.com/",
                Version = "6"
            },
            new SkillEntity()
            {
                Name = "jQuery",
                Category = Categories.FrontEndDevelopment,
                ExperienceYears = 5,
                SourceURL = "https://jquery.com"
            },
            new SkillEntity()
            {
                Name = "CSS",
                Category = Categories.FrontEndDevelopment,
                ExperienceYears = 7,
                SourceURL = "https://www.w3.org/Style/CSS/Overview.en.html"
            },
            new SkillEntity()
            {
                Name = "Visual Studio",
                Category = Categories.DevelopmentTools,
                ActiveLearningDate = new DateTime(2013, 5, 1),
                SourceURL = "https://visualstudio.microsoft.com",
                Version = "17"
            },
            new SkillEntity()
            {
                Name = "JIRA",
                Category = Categories.Tools,
                ExperienceYears = 6,
                SourceURL = "https://www.atlassian.com/software/jira"
            },
            new SkillEntity()
            {
                Name = "REST",
                Category = Categories.Concepts,
                ActiveLearningDate = new DateTime(2014, 5, 1)
            },
            new SkillEntity()
            {
                Name = "SEO",
                Category = Categories.Concepts,
                ExperienceYears = 3
            },
            new SkillEntity()
            {
                Name = "Postman",
                Category = Categories.DevelopmentTools,
                ExperienceYears = 3,
                SourceURL = "https://www.postman.com"
            },
            new SkillEntity()
            {
                Name = "Single Page Applications",
                Category = Categories.Concepts,
                ExperienceYears = 5
            },
            new SkillEntity()
            {
                Name = "Dependency Injection",
                Category = Categories.Concepts,
                ActiveLearningDate = new DateTime(2016, 3, 1)
            },
            new SkillEntity()
            {
                Name = "Responsive Design",
                Category = Categories.Concepts,
                ExperienceYears = 6
            },
            new SkillEntity()
            {
                Name = "Azure",
                Category = Categories.CloudEngineering,
                ActiveLearningDate = new DateTime(2019, 2, 1),
                SourceURL = "https://azure.microsoft.com"
            },
            new SkillEntity()
            {
                Name = "Azure DevOps",
                Category = Categories.DevOps,
                ActiveLearningDate = new DateTime(2019, 11, 1),
                SourceURL = "https://azure.microsoft.com/en-us/solutions/devops"
            },
            new SkillEntity()
            {
                Name = "Application Insights",
                Category = Categories.Tools,
                ActiveLearningDate = new DateTime(2021, 10, 1),
                SourceURL = "https://learn.microsoft.com/en-us/azure/azure-monitor/app/app-insights-overview"
            },
            new SkillEntity()
            {
                Name = "Full Story",
                Category = Categories.Tools,
                ExperienceYears = 1,
                SourceURL = "https://www.fullstory.com"
            },
            new SkillEntity()
            {
                Name = "Microservices",
                Category = Categories.Concepts,
                ActiveLearningDate = new DateTime(2017, 6, 1)
            },
            new SkillEntity()
            {
                Name = "Multithreaded Development",
                Category = Categories.Concepts,
                ActiveLearningDate = new DateTime(2017, 6, 1)
            },
            new SkillEntity()
            {
                Name = "Continuous Integration",
                Category = Categories.DevOps,
                ActiveLearningDate = new DateTime(2018, 5, 1)
            },
            new SkillEntity()
            {
                Name = "Vue",
                Category = Categories.FrontEndDevelopment,
                ExperienceYears = 1,
                SourceURL = "https://vuejs.org"
            },
            new SkillEntity()
            {
                Name = "Docker",
                Category = Categories.CloudEngineering,
                ExperienceYears = 1,
                SourceURL = "https://www.docker.com"
            },
            new SkillEntity()
            {
                Name = "Blazor",
                Category = Categories.FrontEndDevelopment,
                ExperienceYears = 1,
                SourceURL = "https://dotnet.microsoft.com/apps/aspnet/web-apps/blazor"
            },
            new SkillEntity()
            {
                Name = "Kubernetes",
                Category = Categories.CloudEngineering,
                ExperienceYears = 1,
                SourceURL = "https://kubernetes.io"
            },
            new SkillEntity()
            {
                Name = "ARM Templates",
                Category = Categories.CloudEngineering,
                ActiveLearningDate = new DateTime(2020, 10, 1),
                SourceURL = "https://docs.microsoft.com/en-us/azure/azure-resource-manager/templates/overview"
            },
            new SkillEntity()
            {
                Name = "Bicep",
                Category = Categories.CloudEngineering,
                ActiveLearningDate = new DateTime(2023, 6, 1),
                SourceURL = "https://learn.microsoft.com/en-us/azure/azure-resource-manager/bicep/overview?tabs=bicep"
            },
            new SkillEntity()
            {
                Name = "Infrastructure As Code",
                Category = Categories.Concepts,
                ActiveLearningDate = new DateTime(2020, 10, 1)
            },
            new SkillEntity()
            {
                Name = "NoSQL",
                Category = Categories.DatabaseDevelopment,
                ActiveLearningDate = new DateTime(2023, 1, 1),
            },
            new SkillEntity()
            {
                Name = "Powershell",
                Category = Categories.DevelopmentTools,
                ExperienceYears = 1,
                SourceURL = "https://learn.microsoft.com/en-us/powershell/scripting/overview"
            }
        }.OrderByDescending(x => x.GetExperienceMonths());

        public static class Categories
        {
            public const string FrontEndDevelopment = "Front-End Development";
            public const string BackEndDevelopment = "Back-End Development";
            public const string DatabaseDevelopment = "Database Development";
            public const string Tools = "Tools";
            public const string DevelopmentTools = "Development Tools";
            public const string DevOps = "DevOps";
            public const string CloudEngineering = "Cloud Engineering";
            public const string Concepts = "Concepts";
        }

        public static class Levels
        {
            public static int MasterMonths
            {
                get
                {
                    return History.Skip(History.Count() / 4).First().GetExperienceMonths();
                }
            }

            public static int ExpertMonths
            {
                get
                {
                    return History.Skip(History.Count() / 2).First().GetExperienceMonths();
                }
            }

            public static int JourneymanMonths
            {
                get
                {
                    return History.Skip(History.Count() * 3 / 4).First().GetExperienceMonths();
                }
            }
        }
    }
}
