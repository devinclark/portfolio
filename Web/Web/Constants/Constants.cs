﻿namespace DKC.Portfolio.Web
{
    internal static class Constants
    {
        internal static DateTime InstanceStartDate
        {
            get
            {
                if (!_InstanceStartDate.HasValue)
                {
                    _InstanceStartDate = DateTime.UtcNow;
                }

                return _InstanceStartDate.Value;
            }
        }
        private static DateTime? _InstanceStartDate;
    }
}
