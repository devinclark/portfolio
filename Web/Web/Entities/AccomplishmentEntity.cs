﻿using System;

namespace DKC.Portfolio.Web
{
    public class AccomplishmentEntity
    {
        public string ID { get; set; }
        public string Award { get; set; }
        public string Organization { get; set; }
        public string OrganizationURL { get; set; }
        public string OrganizationAvatarURL 
        {
            get
            {
                return $"/images/accomplishments/{ID}.jpg";
            }
        }
        public string Location { get; set; }
        public DateTime Date { get; set; }
    }
}
