﻿using System.Text.RegularExpressions;

namespace DKC.Portfolio.Web
{
    public class EmploymentEntity
    {
        #region Variables

        public string ID => Regex.Replace(Company.Replace(' ', '-'), @"[^A-z/-]", string.Empty).ToLower();
        public string Title { get; set; } = string.Empty;
        public string EmploymentType { get; set; } = string.Empty;
        public string Company { get; set; } = string.Empty;
        public string? CompanyURL { get; set; }
        public string CompanyAvatarURL => $"/images/companies/{ID}.jpg";
        public string Location { get; set; } = string.Empty;
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public IEnumerable<string> Accomplishments { get; set; } = new List<string>();
        public IEnumerable<string> Technologies { get; set; } = new List<string>();
        public IEnumerable<string> Tools { get; set; } = new List<string>();
        public IEnumerable<string> Concepts { get; set; } = new List<string>();

        #endregion

        #region Methods

        public int GetExperienceMonths()
        {
            DateTime endTimeSpan = EndDate ?? DateTime.UtcNow;

            return ((endTimeSpan.Year - StartDate.Year) * 12) + endTimeSpan.Month - StartDate.Month;
        }

        #endregion

        public static class EmploymentTypes
        {
            public const string FullTime = "Full-time";
        }
    }
}
