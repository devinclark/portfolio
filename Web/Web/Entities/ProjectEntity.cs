﻿using System.Text.RegularExpressions;

namespace DKC.Portfolio.Web
{
    public class ProjectEntity
    {
        #region Variables

        public string ID => Regex.Replace(Name.Replace(' ', '-'), @"[^A-z/-]", string.Empty).ToLower();
        public string Name { get; set; } = string.Empty;
        public string? Subtitle { get; set; }
        public string? ApplicationType { get; set; }
        public string Company { get; set; } = string.Empty;
        public string EmploymentCompanyID { get; set; } = string.Empty;
        public string? ProjectURL { get; set; }
        public string? ProjectFriendlyURL => ProjectURL?.Replace("https://", String.Empty)?.Replace("http://", String.Empty)?.Replace("www.", String.Empty)?.ToLower();
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int ImageQuantity { get; set; }
        public IEnumerable<string> ProjectImages
        {
            get
            {
                var projectImages = new List<string>();

                for (int i = 1; i <= ImageQuantity; i++)
                {
                    projectImages.Add($"/images/projects/{ID}/{i}.jpg");
                }

                return projectImages;
            }
        }
        public IEnumerable<string> Technologies { get; set; } = new List<string>();
        public IEnumerable<string> Highlights { get; set; } = new List<string>();

        #endregion
    }
}
