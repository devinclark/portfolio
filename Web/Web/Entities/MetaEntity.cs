﻿using System.Text;

namespace DKC.Portfolio.Web
{
    public class MetaEntity
    {
        #region Variables

        public string Title { get; set; }
        public string TitleSuffix { get; set; }
        public IEnumerable<string> Keywords { get; set; } = new List<string>();
        public string Description { get; set; }
        public string Language { get; set; }
        public string Robots { get; set; }
        public string Canoncial { get; set; }
        public string Author { get; set; }
        public string ShortcutIcon { get; set; }
        public IEnumerable<string> ContentLanguages { get; set; } = new List<string>();

        #endregion

        #region Constructors

        public MetaEntity(bool useDefaultMeta = true)
        {
            if (useDefaultMeta)
            {
                Title = SEO.DefaultMETAData.Title;
                TitleSuffix = SEO.DefaultMETAData.TitleSuffix;
                Keywords = SEO.DefaultMETAData.Keywords;
                Description = SEO.DefaultMETAData.Description;
                Language = SEO.DefaultMETAData.Language;
                Robots = SEO.DefaultMETAData.Robots;
                Canoncial = SEO.DefaultMETAData.Canoncial;
                Author = SEO.DefaultMETAData.Author;
                ShortcutIcon = SEO.DefaultMETAData.ShortcutIcon;
                ContentLanguages = SEO.DefaultMETAData.ContentLanguages;
            }
        }

        #endregion

        #region Methods

        public string GetTitleTagValue()
        {
            StringBuilder sb = new StringBuilder();

            if (!String.IsNullOrEmpty(Title))
            {
                sb.Append(Title);
            }
            
            foreach (string keyword in Keywords)
            {
                if (sb.Length == 0)
                {
                    sb.Append(keyword);
                }
                else if (sb.Length + keyword.Length + 3 <= 70)
                {
                    sb.Append($" | {keyword}");
                }
            }

            if (!String.IsNullOrEmpty(TitleSuffix) && sb.Length + TitleSuffix.Length + 3 <= 70)
            {
                sb.Append($" | {TitleSuffix}");
            }

            return sb.ToString();
        }

        #endregion
    }
}
