﻿using System.Text.RegularExpressions;

namespace DKC.Portfolio.Web
{
    public class SkillEntity
    {
        #region Variables

        public string ID
        {
            get
            {
                return Regex.Replace(Name.Replace(' ', '-'), @"[^A-z/-]", string.Empty).ToLower();
            }
        }
        public string Name { get; set; } = string.Empty;
        public string Category { get; set; } = string.Empty;
        public DateTime? ActiveLearningDate { get; set; }
        public int? ExperienceYears { get; set; }
        public string? SourceURL { get; set; }
        public string? Version { get; set; }
        public bool HasIcon { get; set; } = true;
        public string? IconURL => $"/images/skills/{ID}.svg";

        #endregion

        #region Methods

        public int GetExperienceMonths()
        {
            int months = ExperienceYears.HasValue ? ExperienceYears.Value * 12 : 0;
            
            if (ActiveLearningDate.HasValue)
            {
                months += ((DateTime.UtcNow.Year - ActiveLearningDate.Value.Year) * 12) + DateTime.UtcNow.Month - ActiveLearningDate.Value.Month;
            }

            return months;
        }

        #endregion
    }
}
